//chapter 1 Where are you from ?

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_tts/flutter_tts.dart';

final FlutterTts flutterTts = FlutterTts();
speak(phrase) async {
  await flutterTts.speak(phrase);
}

class Chapter_3 extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return Chapter_3_State();
  }
}

class Chapter_3_State extends State<Chapter_3> {
  @override
  void initState() {
    speak(
        'This is the simple expression about how to introduce yourself');
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Chapter 3 : What's your name ?"),
      ),
      body: Container(
        child: Column(
          children: <Widget>[
            
            DialogContainer()
          
          ],
        ),
      ),
    );
  }
}

class DialogContainer extends StatelessWidget {
  final double buttonHeight = 15.00;
  final double boxHeight = 10.00;
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 30, vertical: 20),
      width: MediaQuery.of(context).size.width,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          RaisedButton(
          padding: EdgeInsets.symmetric(vertical : buttonHeight),
            child: Text(
              'Excuse Me, What is your name ?',
              style: TextStyle(color: Colors.white),
            ),
            color: Colors.indigo[900],
            onPressed: () => {speak('Excuse Me, What is your name ?')},
          ),
          RaisedButton(
          padding: EdgeInsets.symmetric(vertical : buttonHeight),
            child: Text(
              "My name is Otto. What's your name?",
              style: TextStyle(color: Colors.white),
            ),
            color: Colors.blueAccent,
            onPressed: () => {speak("My name is Otto. What's your name?")},
          ),
          RaisedButton(
          padding: EdgeInsets.symmetric(vertical : buttonHeight),
            child: Text(
              'My name is ',
              style: TextStyle(color: Colors.white),
            ),
            color: Colors.indigo[900],
            onPressed: () => {speak('My name is ')},
          ),
          RaisedButton(
          padding: EdgeInsets.symmetric(vertical : buttonHeight),
            child: Text(
              "Oh! You speak English very well.",
              style: TextStyle(color: Colors.white),
            ),
            color: Colors.blueAccent,
            onPressed: () => {speak('Oh! You speak English very well.')},
          ),
          RaisedButton(
          padding: EdgeInsets.symmetric(vertical : buttonHeight),
            child: Text(
              'Thank You',
              style: TextStyle(color: Colors.white),
            ),
            color: Colors.indigo[900],
            onPressed: () => {speak('Thank You')},
          ),

        ],
      ),
    );
  }
}
