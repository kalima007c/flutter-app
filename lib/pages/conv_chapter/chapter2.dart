//chapter 1 Where are you from ?

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_tts/flutter_tts.dart';

final FlutterTts flutterTts = FlutterTts();
speak(phrase) async {
  await flutterTts.speak(phrase);
}

class Chapter_2 extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return Chapter_2_State();
  }
}

class Chapter_2_State extends State<Chapter_2> {
  @override
  void initState() {
    speak(
        'This is the lesson about asking someone where is him or her come from');
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Chapter 2 : Where are you from?"),
      ),
      body: Container(
        child: Column(
          children: <Widget>[
            
            DialogContainer()
          
          ],
        ),
      ),
    );
  }
}

class DialogContainer extends StatelessWidget {
  final double buttonHeight = 15.00;
  final double boxHeight = 10.00;
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 30, vertical: 20),
      width: MediaQuery.of(context).size.width,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          RaisedButton(
          padding: EdgeInsets.symmetric(vertical : buttonHeight),
            child: Text(
              'Excuse Me, Are you American ?',
              style: TextStyle(color: Colors.white),
            ),
            color: Colors.indigo[900],
            onPressed: () => {speak('Excuse Me,Are you American ?')},
          ),
          RaisedButton(
          padding: EdgeInsets.symmetric(vertical : buttonHeight),
            child: Text(
              'No',
              style: TextStyle(color: Colors.white),
            ),
            color: Colors.blueAccent,
            onPressed: () => {speak('No.')},
          ),
          RaisedButton(
          padding: EdgeInsets.symmetric(vertical : buttonHeight),
            child: Text(
              'Do you speak English ?',
              style: TextStyle(color: Colors.white),
            ),
            color: Colors.indigo[900],
            onPressed: () => {speak('Do you speak English ?')},
          ),
          RaisedButton(
          padding: EdgeInsets.symmetric(vertical : buttonHeight),
            child: Text(
              'A little bit, but not well.',
              style: TextStyle(color: Colors.white),
            ),
            color: Colors.blueAccent,
            onPressed: () => {speak('A little bit,but not well.')},
          ),
          RaisedButton(
          padding: EdgeInsets.symmetric(vertical : buttonHeight),
            child: Text(
              'How long have you been there ?',
              style: TextStyle(color: Colors.white),
            ),
            color: Colors.indigo[900],
            onPressed: () => {speak('How long have you been there ?')},
          ),
          RaisedButton(
          padding: EdgeInsets.symmetric(vertical : buttonHeight),
            child: Text(
              'I have been here for two months.',
              style: TextStyle(color: Colors.white),
            ),
            color: Colors.blueAccent,
            onPressed: () => {speak('I have been here for two months.')},
          ),
          RaisedButton(
          padding: EdgeInsets.symmetric(vertical : buttonHeight),
            child: Text(
              'What do you do for work ?',
              style: TextStyle(color: Colors.white),
            ),
            color: Colors.indigo[900],
            onPressed: () => {speak('What do you do for work ?')},
          ),
          RaisedButton(
          padding: EdgeInsets.symmetric(vertical : buttonHeight),
            child: Text(
              'I am student',
              style: TextStyle(color: Colors.white),
            ),
            color: Colors.blueAccent,
            onPressed: () => {speak('I am studen.')},
          ),
        ],
      ),
    );
  }
}
