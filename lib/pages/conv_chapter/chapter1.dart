//chapter 1 Where are you from ?

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_tts/flutter_tts.dart';

final FlutterTts flutterTts = FlutterTts();
speak(phrase) async {
  await flutterTts.speak(phrase);
}

class Chapter_1 extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return Chapter_1_State();
    
  }
}

class Chapter_1_State extends State<Chapter_1> {
  @override
  void initState() {
    speak(
        'This is the first lesson about how to speak a simple daily expression');
  }
  
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Chapter 1 : Where are you from?"),
      ),
      body: Container(
        child: Column(
          children: <Widget>[DialogContainer()],
        ),
      ),
    );
  }
}

class DialogContainer extends StatelessWidget {
  final double buttonHeight = 15.00;
  final double boxHeight = 10.00;
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 30, vertical: 20),
      width: MediaQuery.of(context).size.width,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          RaisedButton(
          padding: EdgeInsets.symmetric(vertical : buttonHeight),
            child: Text(
              'Hello!',
              style: TextStyle(color: Colors.white),
            ),
            color: Colors.indigo[900],
            onPressed: () => {speak('Hello!')},
          ),
          RaisedButton(
          padding: EdgeInsets.symmetric(vertical : buttonHeight),
            child: Text(
              'Hi!',
              style: TextStyle(color: Colors.white),
            ),
            color: Colors.blueAccent,
            onPressed: () => {speak('Hi!')},
          ),
          RaisedButton(
          padding: EdgeInsets.symmetric(vertical : buttonHeight),
            child: Text(
              'How are you ?',
              style: TextStyle(color: Colors.white),
            ),
            color: Colors.indigo[900],
            onPressed: () => {speak('How are you ?')},
          ),
          RaisedButton(
          padding: EdgeInsets.symmetric(vertical : buttonHeight),
            child: Text(
              'I am good. How are you ?',
              style: TextStyle(color: Colors.white),
            ),
            color: Colors.blueAccent,
            onPressed: () => {speak('I am good. How are you ?')},
          ),
          RaisedButton(
          padding: EdgeInsets.symmetric(vertical : buttonHeight),
            child: Text(
              'Good. Do you speak English ?',
              style: TextStyle(color: Colors.white),
            ),
            color: Colors.indigo[900],
            onPressed: () => {speak('Good. Do you speak English ?')},
          ),
          RaisedButton(
          padding: EdgeInsets.symmetric(vertical : buttonHeight),
            child: Text(
              'A little bit.',
              style: TextStyle(color: Colors.white),
            ),
            color: Colors.blueAccent,
            onPressed: () => {speak('A little bit.')},
          ),
          RaisedButton( 
          padding: EdgeInsets.symmetric(vertical : buttonHeight),
            child: Text(
              'Where are you from ?',
              style: TextStyle(color: Colors.white),
            ),
            color: Colors.indigo[900],
            onPressed: () => {speak('Where are you from ?')},
          ),
          RaisedButton(
          padding: EdgeInsets.symmetric(vertical : buttonHeight),
            child: Text(
              'I am from Thailand.',
              style: TextStyle(color: Colors.white),
            ),
            color: Colors.blueAccent,
            onPressed: () => {speak('I am from Thailand')},
          ),
          RaisedButton(
          padding: EdgeInsets.symmetric(vertical : buttonHeight),
            child: Text(
              'Nice to meet you.',
              style: TextStyle(color: Colors.white),
            ),
            color: Colors.indigo[900],
            onPressed: () => {speak('Nice to meet you')},
          ),
          RaisedButton(
          padding: EdgeInsets.symmetric(vertical : buttonHeight),
            child: Text(
              'Nice to meet you too.',
              style: TextStyle(color: Colors.white),
            ),
            color: Colors.blueAccent,
            onPressed: () => {speak('Nice to meet you too')},
          ),
        ],
      ),
    );
  }
}
